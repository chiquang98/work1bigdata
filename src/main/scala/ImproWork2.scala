import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.functions.{broadcast, col, collect_set, expr, input_file_name}
import org.apache.spark.sql.{Column, DataFrame, SaveMode, SparkSession}
import java.nio.file.{FileSystems, Files}
import scala.collection.JavaConverters._
import java.nio.file.{Files, Paths}
import scala.io.Source.fromFile
object ImproWork2 {
  def handleNullDF(sparkSession: SparkSession,df:DataFrame):DataFrame={
    import sparkSession.implicits._
    val listCol = df.columns
    var preValue = "null"
    val pivot2 = df.map(f=>{
      preValue = "null"
      var rowVal  = List[String]()
      rowVal:+=f.getAs[String](listCol(0))
      listCol.foreach(k=>{
        if(listCol.indexOf(k)>=1){
          var currRow = f.getAs[String](k)//Giá trị của cột thứ K >=1
          //Nếu giá trị row là null thì mình thêm giá trị của cột trước đó
          if(currRow==null||currRow.compareToIgnoreCase("null")==0){// so sánh == null vì kiểu dữ liệu là null do phép join chứ không phải giá trị là null
            //=> cần so sánh kiểu == null và giá trị == null (do mình khởi tạo)
            rowVal:+=preValue
          }
          else{
            //nếu không thêm giá trị cột hiện tại
            preValue = currRow
            rowVal:+=currRow
          }
        }
      })
      rowVal
    })
    def getColAtIndex(id:Int): Column = col(s"value")(id).as(listCol(id))
    val len = listCol.length-1
    val columns: IndexedSeq[Column] = (0 to len).map(getColAtIndex)
    val finalRs = pivot2.select(columns:_*)
    val finalRs1 = finalRs.filter(!(col(listCol(len))==="Free"))
    finalRs1
  }
  def sortByMin(s1: String, s2: String) = {
    //need Try catch
    var min1 = s1.substring(10,12).toInt
    var min2 = s2.substring(10,12).toInt
    min1 < min2
  }
  def handleFile1(sparkSession: SparkSession,dataFrame: DataFrame):DataFrame = {
    import sparkSession.implicits._
    val t0 = System.nanoTime()
    val df12 = dataFrame.drop("_c1")
    val nextDF = df12.toDF("Timestamp","MessageType","PhoneNumber","IPAddress")
    val nextDF1 = nextDF.filter(col("IPAddress").isNotNull && col("PhoneNumber").isNotNull && col("Timestamp").isNotNull && col("MessageType").isNotNull
      && !col("Timestamp").contains(" ")).map(f=>{
      val IPAddress = f.getAs[String]("IPAddress").split("\\[")
      var timestamp = f.getAs[String]("Timestamp")
      timestamp = timestamp.substring(0,12)
      val msgType = f.getAs[String]("MessageType")
      var phoneNumber = f.getAs[String]("PhoneNumber")
      if(msgType.compareToIgnoreCase("Stop")==0){
        phoneNumber = "Free"
      }
      (timestamp,phoneNumber,IPAddress(0),msgType)
    })
    val nextDF2 = nextDF1.toDF("Timestamp","PhoneNumber","IPAddress","MessageType")
    var columnDF2 = nextDF2.select(collect_set("Timestamp")).first().getList[String](0).asScala.toList.sortWith(sortByMin)
    val pivot1 = nextDF2.groupBy("IPAddress").
      pivot("Timestamp",columnDF2)
      .agg(expr("coalesce(last(PhoneNumber), \"null\")"))
    //Một IP trong 1 khoảng thời gian có thể xuất hiện lớn hơn 2 lần
    //Xử lý dựa vào messageType, vì thằng Start mới trong timeline thời gian sẽ nằm sau (cuối) nên lấy last để lấy số thằng cuối điền vào thời gian
    val finalRs = handleNullDF(sparkSession,pivot1)
    val t1 = System.nanoTime()
    finalRs
  }
  def mergeTwoHours1(sparkSession: SparkSession,df1:DataFrame,df2:DataFrame): DataFrame ={
    var columnDF2 = df2.columns
    var columnDF1 = df1.columns
    val lenColumnDF1 = columnDF1.length
    val lenColumnDF2 = columnDF2.length
    val df11 = df1.select("IPAddress",columnDF1(lenColumnDF1-1))
    var arrColumnOrderd = columnDF2.slice(1,lenColumnDF2)
    arrColumnOrderd+:=columnDF1(lenColumnDF1-1)
    arrColumnOrderd+:=columnDF2(0)
    var columnDrop = df11.columns.filter(_.compareToIgnoreCase("IPAddress")!=0)
    val dfJoin = df11.repartition(200,col("IPAddress")).
      join(df2.repartition(200,col("IPAddress")),Seq("IPAddress"),"fullouter")
    val dfOutput = handleNullDF(sparkSession,dfJoin)
    val dfOuputFinal =dfOutput.drop(columnDF1(lenColumnDF1-1))
    dfOuputFinal
  }
  def readFile(sparkSession: SparkSession,pathInput:String,pathOutput:String,pathLog:String,pathLogRecently:String): Unit ={
    var flag = true
    var numFileHandled = 0

    while(true){
      //cứ đọc đi đọc lại liên tục
      val dir = FileSystems.getDefault.getPath(pathInput)
      val listDir = Files.walk(dir).iterator().asScala.filter(Files.isRegularFile(_)).toList
      var logs = fromFile(pathLog).getLines.toList//file phải có trước nếu không sẽ lỗi
      var logPathRecently = fromFile(pathLogRecently).mkString
      if(logs.length>2){
        flag = false
      }
      if(listDir.length==2){
        //nếu chỉ có 1 file thì handle rồi lưu vào output file 0 giờ, hoặc tên file của file đầu tiên đó
        val pathFile = listDir(0).toString
        var index = pathFile.lastIndexOf("\\")
        var nameFile = pathFile.substring(index+1,index+12)
        val df = sparkSession.read.options(Map("delimiter"->"|")).csv(pathInput)
        //Vì có 2 file đầu tiên nên đọc luôn 2 file
        val dfHandle = handleFile1(sparkSession,df)
        dfHandle.write.mode(SaveMode.Overwrite).parquet(pathOutput+nameFile)
        writeLog(pathLog, listDir(0).toString, listDir(1).toString)
        //ghi tên giờ vừa xử lý xong để nếu crash thì sẽ lấy kết quả giờ đó
        writeLogRecently(pathLogRecently,listDir(0).toString)
        //sau đó ghi log là file này đã lưu
        numFileHandled+=2
      }
      else if(listDir.length>2){//nếu số lượng tệp nhiều hơn 1, thì sẽ nhảy vào đây, file đầu tiên đã được lưu
        val pairHour = listDir.grouped(2).toList
        pairHour.foreach(f=>{//loop here
          logPathRecently = fromFile(pathLogRecently).mkString //lấy lại tên giờ mới nhất đã xử lý
          //check xem cặp giờ 0 đầu tiên xử lý chưa, nếu rồi thì không xét,
          //lấy cái mới nhất trong ouput rồi merge
          val path1 = f(0).toString
          val path2 = f(1).toString
          var index = path1.lastIndexOf("\\")
          var nameFile = path1.substring(index+1,index+12)
          if(logs.filter(_.contains(path1)).length==0){//Nếu file chưa có trong log thì xử lý file
            val dfN = sparkSession.read.options(Map("delimiter"->"|")).csv(path1,path2)
            val dfHandle = handleFile1(sparkSession,dfN)
            if(flag==true){//cờ flag để kiểm tra xử lý file lần đầu, khi mới đọc giờ đầu tiên thì chỉ cần handleFile
              //chưa có thêm giờ nên sẽ không có mergeFile tại hàm này
              dfHandle.write.mode(SaveMode.Overwrite).parquet(pathOutput+nameFile)
              writeLog(pathLog, path1, path2)
              writeLogRecently(pathLogRecently,path1)
              flag = false
            }
            else{//Nếu file vừa lưu chưa có trong log và file không phải giờ đầu thì sẽ cần merge giờ cữ để ra giờ mới
              var index = logPathRecently.lastIndexOf("\\")
              val nameHour = logPathRecently.substring(index+1,index+12)
              val dfTemp = sparkSession.read.parquet(pathOutput+nameHour)
              var dfOutput = mergeTwoHours1(sparkSession,dfTemp,dfHandle)//merge giờ cũ và giờ mới
//              println(nameFile)
              dfOutput.write.mode("overwrite").parquet(pathOutput+nameFile)
              writeLog(pathLog, path1, path2)
              writeLogRecently(pathLogRecently,path1)
            }
          }
        })
      }
//    Thread.sleep(2000)
    }
  }
  //ghi lại đường dẫn những file input đã xử lý để khi đọc lại không xử lý lại những file này nữa
  def writeLog(pathLog:String,path1:String,path2:String): Unit ={
    reflect.io.File(pathLog).appendAll(path1)
    reflect.io.File(pathLog).appendAll("\n")
    reflect.io.File(pathLog).appendAll(path2)
    reflect.io.File(pathLog).appendAll("\n")
  }
  //ghi lại đường dẫn lưu output mới nhất -> nếu crash sẽ lấy ouput này để join với file input đầu vào
  def writeLogRecently(pathLogRecently:String,path:String): Unit ={
    reflect.io.File(pathLogRecently).writeAll(path)
  }
  def main(args: Array[String]): Unit = {
    Logger.getLogger("org").setLevel(Level.ERROR)
    val sparkSession:SparkSession = SparkSession.builder()
      .appName("test")
      .config("spark.sql.inMemoryColumnarStorage.compressed", true)
      .config("spark.sql.shuffle.partitions",30)
      .config("spark.sql.autoBroadcastJoinThreshold",-1)
      .master("local[6]")
      .getOrCreate()
    val pathLog ="E:/Tài liệu/Big Data/Log.txt"
    val pathInput = "E:\\Tài liệu\\Big Data\\mdo"
    val pathOutput = "E:/Tài liệu/Big Data/output3/"
    val pathLogRecently = "E:/Tài liệu/Big Data/LogRecently.txt"
    readFile(sparkSession,pathInput,pathOutput,pathLog,pathLogRecently)

//    val threadReadFile = new Thread {
//      readFile(sparkSession,pathInput,pathOutput,pathLog,pathLogRecently)
//    }
//    threadReadFile.start()
  }
}
