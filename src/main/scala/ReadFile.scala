import java.nio.file.{FileSystems, Files, Paths}

import Work3.{handleFile1, mergeTwoHours1}
import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.{SaveMode, SparkSession}

import scala.collection.JavaConverters.asScalaIteratorConverter
import scala.io.Source._
import scala.reflect.io.File
object ReadFile {
  def main(args: Array[String]): Unit = {
    Logger.getLogger("org").setLevel(Level.ERROR)
    val sparkSession:SparkSession = SparkSession.builder()
      .appName("test")
      .master("local[6]")
      .getOrCreate()
//    val df = sparkSession.read.parquet("E:\\Tài liệu\\Big Data\\ouput2\\20201222_00")
//    df.show(5,false)
    val path1 = "E:\\Tài liệu\\Big Data\\20201221_090000_mdo-feyha01c_raw_RadiusBroadcaster.gpb.gz.tmp.csv"
    val index = path1.lastIndexOf("\\")
    val valHour = path1.substring(index+1,index+12)
    val listVal = valHour.split("_")
    var hour = listVal.head+listVal(1)
    println(hour)
    val df = sparkSession.read.options(Map("delimiter"->"|")).csv(path1).filter(col("_c0").contains(hour))
    df.show(5,false)




  }
  def createFileLog(pathLog:String,pathLogRecently:String): Unit ={
    try{
      val confFolderLogRecently = Paths.get(pathLogRecently)
      val confFileLogRecently = Paths.get(pathLogRecently+"/LogRecently.log")
      val confFolderLog = Paths.get(pathLog)
      val confFileLog= Paths.get(pathLog+"/Log.log")
      val fileLogRecentlyExist = Files.exists(Paths.get(pathLogRecently+"/LogRecently.log"))
      val fileLogExist = Files.exists(Paths.get(pathLog+"/Log.log"))
      if(!fileLogExist){
        Files.createFile(Files.createDirectories(confFolderLog).resolve(confFileLog.getFileName))
      }
      if(!fileLogRecentlyExist){
        //Nếu file chưa tồn tại thì tạo file
        Files.createFile(Files.createDirectories(confFolderLogRecently).resolve(confFileLogRecently.getFileName))
      }
    }
    catch{
      case e:Exception=>{
        println(e.getCause.getMessage)
      }
    }
  }
  def readFile(sparkSession: SparkSession,pathInput:String,pathOutput:String,pathLog:String,pathLogRecently:String): Unit ={
    createFileLog(pathLog, pathLogRecently)
    var flag = true
    var prePathInput = "init"
    var isFirst = true
    var listPathHourGroup = List[String]()
    while(true){
      //cứ đọc đi đọc lại liên tục
      try{
        val dir = FileSystems.getDefault.getPath(pathInput)
        val listDir = Files.walk(dir).iterator().asScala.filter(Files.isRegularFile(_)).toList//Danh sách đường dẫn input đầu vào
        var logs = fromFile(pathLog+"/Log.log").getLines.toList
        var logPathRecently = fromFile(pathLogRecently+"/LogRecently.log").mkString
        if(logs.nonEmpty){//Nếu file logs chứa đường dẫn file tức là đã xử lý rồi, flag = false để đánh dấu không phải xử lý lần đầu
          flag = false
        }
        if(listDir.nonEmpty&&logs.isEmpty){//đọc các file của giờ đầu tiên
          isFirst = true
          listDir.foreach(f=>{
              var index = f.toString.lastIndexOf("\\")
              var valHour = f.toString.substring(index+1,index+12)
              if(isFirst){
                prePathInput = valHour
                isFirst = false
                listPathHourGroup:+=f.toString
              }
              else{
                if(valHour.compareToIgnoreCase(prePathInput)==0){
                  //nếu vẫn còn đường dẫn của giờ trước
                  listPathHourGroup:+=f.toString
                }
              }
          })
          val pathFile = listPathHourGroup.head
          var index = pathFile.lastIndexOf("\\")
          var nameFile = pathFile.substring(index+1,index+12)
          val df = sparkSession.read.options(Map("delimiter"->"|")).csv(listPathHourGroup:_*)
          val dfHandle = handleFile1(sparkSession,df,nameFile)
          dfHandle.write.mode(SaveMode.Overwrite).parquet(pathOutput+"/"+nameFile)
          writeLog(pathLog,listPathHourGroup)
          //ghi tên giờ vừa xử lý xong để nếu crash thì sẽ lấy kết quả giờ đó
          writeLogRecently(pathLogRecently,pathFile)
          listPathHourGroup = List[String]()
          //sau đó ghi log là file này đã lưu
        }
        else if(listDir.nonEmpty&&logs.nonEmpty) {//Nếu đã có input đầu vào, và file logRecently đã có nội dung
          listPathHourGroup = List[String]()
          prePathInput = "init"
          isFirst = true
          listDir.foreach(f=>{
            var index = f.toString.lastIndexOf("\\")
            var valHour = f.toString.substring(index+1,index+12)
            logPathRecently = fromFile(pathLogRecently+"/LogRecently.log").mkString //lấy lại tên giờ mới nhất đã xử lý
            if(!logs.contains(f.toString)){//Nếu file chưa có trong log thì xử lý
              if(isFirst){
                isFirst = false
                prePathInput = valHour
                listPathHourGroup:+=f.toString
              }
              else{
                if(valHour.compareToIgnoreCase(prePathInput)==0){
                  listPathHourGroup:+=f.toString
                  //end with foreach
                }
                else{
                  //đã sang giờ mới rồi, xử lý giờ cũ và reset listPathHourGroup
                  var index = logPathRecently.lastIndexOf("\\")
                  val nameHour = logPathRecently.substring(index+1,index+12)
                  val dfPre = sparkSession.read.parquet(pathOutput+"/"+nameHour)
                  val dfHourFiles = sparkSession.read.options(Map("delimiter"->"|")).csv(listPathHourGroup:_*)
                  val dfCurr = handleFile1(sparkSession,dfHourFiles,nameHour)
                  var dfOutput = mergeTwoHours1(sparkSession,dfPre,dfCurr)//merge giờ cũ và giờ mới
                  dfOutput.write.mode("overwrite").parquet(pathOutput+"/"+prePathInput)
                  writeLog(pathLog,listPathHourGroup)
                  writeLogRecently(pathLogRecently,listPathHourGroup.head)
                  //Phần giờ sau
                  listPathHourGroup = List[String]()
                  listPathHourGroup:+=f.toString
                  prePathInput = valHour
                }
              }
            }
          })
          //Trường hợp không có file mới, endwithforech
          if(listPathHourGroup.nonEmpty){
            var index = logPathRecently.lastIndexOf("\\")
            val nameHour = logPathRecently.substring(index+1,index+12)
            val dfPre = sparkSession.read.parquet(pathOutput+"/"+nameHour)
            val dfHourFiles = sparkSession.read.options(Map("delimiter"->"|")).csv(listPathHourGroup:_*)
            val dfCurr = handleFile1(sparkSession,dfHourFiles,nameHour)
            var dfOutput = mergeTwoHours1(sparkSession,dfPre,dfCurr)//merge giờ cũ và giờ mới

            var index1 = listPathHourGroup.head.lastIndexOf("\\")
            val namNewHour = listPathHourGroup.head.substring(index1+1,index1+12)
            dfOutput.write.mode("overwrite").parquet(pathOutput+"/"+namNewHour)
            writeLog(pathLog,listPathHourGroup)
            writeLogRecently(pathLogRecently,listPathHourGroup.head)
            listPathHourGroup = List[String]()
          }
        }
      }
      catch {
        case e:Exception=>{
          println(e.getCause.getMessage)
        }
      }
    }
  }
  //ghi lại đường dẫn những file input đã xử lý để khi đọc lại không xử lý lại những file này nữa
  def writeLog(pathLog:String,listPath:List[String]): Unit ={
    listPath.foreach(path=>{
      reflect.io.File(pathLog).appendAll(path)
      reflect.io.File(pathLog).appendAll("\n")
    })
  }
  //ghi lại đường dẫn lưu output mới nhất -> nếu crash sẽ lấy ouput này để join với file input đầu vào
  def writeLogRecently(pathLogRecently:String,path:String): Unit ={
    reflect.io.File(pathLogRecently).writeAll(path)
  }
}
