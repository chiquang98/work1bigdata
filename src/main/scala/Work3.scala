import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.functions.{broadcast, col, collect_set, expr, input_file_name}
import org.apache.spark.sql.{Column, DataFrame, SaveMode, SparkSession}
import java.nio.file.{FileSystems, Files}
import scala.collection.JavaConverters._
import java.nio.file.{Files, Paths}

import scala.io.Source.fromFile
object Work3 {
  def handleNullDF(sparkSession: SparkSession,df:DataFrame):DataFrame={
    import sparkSession.implicits._
    val listCol = df.columns
    var preValue = "null"
    val pivot2 = df.map(f=>{
      preValue = "null"
      var rowVal  = List[String]()
      rowVal:+=f.getAs[String](listCol(0))
      listCol.foreach(k=>{
        if(listCol.indexOf(k)>=1){
          var currRow = f.getAs[String](k)//Giá trị của cột thứ K >=1
          //Nếu giá trị row là null thì mình thêm giá trị của cột trước đó
          if(currRow==null||currRow.compareToIgnoreCase("null")==0){// so sánh == null vì kiểu dữ liệu là null do phép join chứ không phải giá trị là null
            //=> cần so sánh kiểu == null và giá trị == null (do mình khởi tạo)
            rowVal:+=preValue
          }
          else{
            //nếu không thêm giá trị cột hiện tại
            preValue = currRow
            rowVal:+=currRow
          }
        }
      })
      rowVal
    })
    def getColAtIndex(id:Int): Column = col(s"value")(id).as(listCol(id))
    val len = listCol.length-1
    val columns: IndexedSeq[Column] = (0 to len).map(getColAtIndex)
    val finalRs = pivot2.select(columns:_*)
    val finalRs1 = finalRs.filter(!(col(listCol(len))==="Free"))
    finalRs1
  }
  def sortByMin(s1: String, s2: String) = {
    //need Try catch
    var min1 = s1.substring(10,12).toInt
    var min2 = s2.substring(10,12).toInt
    min1 < min2
  }
  def handleFile1(sparkSession: SparkSession,dataFrame: DataFrame,valHour:String):DataFrame = {
    import sparkSession.implicits._
    //filter để bỏ giá trị lỗi
    val listVal = valHour.split("_")
    var hour = listVal.head+listVal(1)
    val dataFrame1 = dataFrame.filter(col("_c0").contains(hour))

    val t0 = System.nanoTime()
    val df12 = dataFrame.drop("_c1")
    val nextDF = df12.toDF("Timestamp","MessageType","PhoneNumber","IPAddress")
    val nextDF1 = nextDF.filter(col("IPAddress").isNotNull && col("PhoneNumber").isNotNull && col("Timestamp").isNotNull && col("MessageType").isNotNull
      && !col("Timestamp").contains(" ")).map(f=>{
      val IPAddress = f.getAs[String]("IPAddress").split("\\[")
      var timestamp = f.getAs[String]("Timestamp")
      timestamp = timestamp.substring(0,12)
      val msgType = f.getAs[String]("MessageType")
      var phoneNumber = f.getAs[String]("PhoneNumber")
      if(msgType.compareToIgnoreCase("Stop")==0){
        phoneNumber = "Free"
      }
      (timestamp,phoneNumber,IPAddress(0),msgType)
    })
    val nextDF2 = nextDF1.toDF("Timestamp","PhoneNumber","IPAddress","MessageType")
    var columnDF2 = nextDF2.select(collect_set("Timestamp")).first().getList[String](0).asScala.toList.sortWith(sortByMin)
    val pivot1 = nextDF2.groupBy("IPAddress").
      pivot("Timestamp",columnDF2)
      .agg(expr("coalesce(last(PhoneNumber), \"null\")"))
    //Một IP trong 1 khoảng thời gian có thể xuất hiện lớn hơn 2 lần
    //Xử lý dựa vào messageType, vì thằng Start mới trong timeline thời gian sẽ nằm sau (cuối) nên lấy last để lấy số thằng cuối điền vào thời gian
    val finalRs = handleNullDF(sparkSession,pivot1)
    val t1 = System.nanoTime()
    finalRs
  }
  def mergeTwoHours1(sparkSession: SparkSession,df1:DataFrame,df2:DataFrame): DataFrame ={
    var columnDF2 = df2.columns
    var columnDF1 = df1.columns
    val lenColumnDF1 = columnDF1.length
    val lenColumnDF2 = columnDF2.length
    val df11 = df1.select("IPAddress",columnDF1(lenColumnDF1-1))
    var arrColumnOrderd = columnDF2.slice(1,lenColumnDF2)
    arrColumnOrderd+:=columnDF1(lenColumnDF1-1)
    arrColumnOrderd+:=columnDF2(0)
    var columnDrop = df11.columns.filter(_.compareToIgnoreCase("IPAddress")!=0)
    val dfJoin = df11.repartition(200,col("IPAddress")).
      join(df2.repartition(200,col("IPAddress")),Seq("IPAddress"),"fullouter")
    val dfOutput = handleNullDF(sparkSession,dfJoin)
    val dfOuputFinal =dfOutput.drop(columnDF1(lenColumnDF1-1))
    dfOuputFinal
  }
  def createFileLog(pathLog:String,pathLogRecently:String): Unit ={
    try{
      val confFolderLogRecently = Paths.get(pathLogRecently)
      val confFileLogRecently = Paths.get(pathLogRecently+"/LogRecently.log")
      val confFolderLog = Paths.get(pathLog)
      val confFileLog= Paths.get(pathLog+"/Log.log")
      val fileLogRecentlyExist = Files.exists(Paths.get(pathLogRecently+"/LogRecently.log"))
      val fileLogExist = Files.exists(Paths.get(pathLog+"/Log.log"))
      if(!fileLogExist){
        Files.createFile(Files.createDirectories(confFolderLog).resolve(confFileLog.getFileName))
      }
      if(!fileLogRecentlyExist){
        //Nếu file chưa tồn tại thì tạo file
        Files.createFile(Files.createDirectories(confFolderLogRecently).resolve(confFileLogRecently.getFileName))
      }
    }
    catch{
      case e:Exception=>{
        println(e.getCause.getMessage)
      }
    }
  }
  def readFile(sparkSession: SparkSession,pathInput:String,pathOutput:String,pathLog:String,pathLogRecently:String): Unit ={
    createFileLog(pathLog, pathLogRecently)
    var flag = true
    var prePathInput = "init"
    var isFirst = true
    var listPathHourGroup = List[String]()
    val pathFileLog = pathLog + "/Log.log"
    val pathFileRecentlyLog = pathLogRecently + "/LogRecently.log"
    while(true){
      //cứ đọc đi đọc lại liên tục
      try{
        val dir = FileSystems.getDefault.getPath(pathInput)
        val listDir = Files.walk(dir).iterator().asScala.filter(Files.isRegularFile(_)).toList//Danh sách đường dẫn input đầu vào
        var logs = fromFile(pathFileLog).getLines.toList
        var logPathRecently = fromFile(pathFileRecentlyLog).mkString
        if(logs.nonEmpty){//Nếu file logs chứa đường dẫn file tức là đã xử lý rồi, flag = false để đánh dấu không phải xử lý lần đầu
          flag = false
        }
        if(listDir.nonEmpty&&logs.isEmpty){//đọc các file của giờ đầu tiên
          isFirst = true
          listDir.foreach(f=>{
            var index = f.toString.lastIndexOf("\\")
            var valHour = f.toString.substring(index+1,index+12)
            if(isFirst){
              prePathInput = valHour
              isFirst = false
              listPathHourGroup:+=f.toString
            }
            else{
              if(valHour.compareToIgnoreCase(prePathInput)==0){
                //nếu vẫn còn đường dẫn của giờ trước
                listPathHourGroup:+=f.toString
              }
            }
          })
          val pathFile = listPathHourGroup.head
          var index = pathFile.lastIndexOf("\\")
          var nameFile = pathFile.substring(index+1,index+12)
          val df = sparkSession.read.options(Map("delimiter"->"|")).csv(listPathHourGroup:_*)
          val dfHandle = handleFile1(sparkSession,df,nameFile)
          dfHandle.write.mode(SaveMode.Overwrite).parquet(pathOutput+"/"+nameFile)
          writeLog(pathFileLog,listPathHourGroup)
          //ghi tên giờ vừa xử lý xong để nếu crash thì sẽ lấy kết quả giờ đó
          writeLogRecently(pathFileRecentlyLog,pathFile)
          listPathHourGroup = List[String]()
          //sau đó ghi log là file này đã lưu
        }
        else if(listDir.nonEmpty&&logs.nonEmpty) {//Nếu đã có input đầu vào, và file logRecently đã có nội dung
          listPathHourGroup = List[String]()
          prePathInput = "init"
          isFirst = true
          listDir.foreach(f=>{
            var index = f.toString.lastIndexOf("\\")
            var valHour = f.toString.substring(index+1,index+12)
            logPathRecently = fromFile(pathFileRecentlyLog).mkString //lấy lại tên giờ mới nhất đã xử lý
            if(!logs.contains(f.toString)){//Nếu file chưa có trong log thì xử lý
              if(isFirst){
                isFirst = false
                prePathInput = valHour
                listPathHourGroup:+=f.toString
              }
              else{
                if(valHour.compareToIgnoreCase(prePathInput)==0){
                  listPathHourGroup:+=f.toString
                  //end with foreach
                }
                else{
                  //đã sang giờ mới rồi, xử lý giờ cũ và reset listPathHourGroup
                  var index = logPathRecently.lastIndexOf("\\")
                  val nameHour = logPathRecently.substring(index+1,index+12)
                  val dfPre = sparkSession.read.parquet(pathOutput+"/"+nameHour)
                  val dfHourFiles = sparkSession.read.options(Map("delimiter"->"|")).csv(listPathHourGroup:_*)
                  val dfCurr = handleFile1(sparkSession,dfHourFiles,nameHour)
                  var dfOutput = mergeTwoHours1(sparkSession,dfPre,dfCurr)//merge giờ cũ và giờ mới
                  dfOutput.write.mode("overwrite").parquet(pathOutput+"/"+prePathInput)
                  writeLog(pathFileLog,listPathHourGroup)
                  writeLogRecently(pathFileRecentlyLog,listPathHourGroup.head)
                  //Phần giờ sau
                  listPathHourGroup = List[String]()
                  listPathHourGroup:+=f.toString
                  prePathInput = valHour
                }
              }
            }
          })
          //Trường hợp không có file mới, endwithforech
          if(listPathHourGroup.nonEmpty){
            var index = logPathRecently.lastIndexOf("\\")
            val nameHour = logPathRecently.substring(index+1,index+12)
            val dfPre = sparkSession.read.parquet(pathOutput+"/"+nameHour)
            val dfHourFiles = sparkSession.read.options(Map("delimiter"->"|")).csv(listPathHourGroup:_*)
            val dfCurr = handleFile1(sparkSession,dfHourFiles,nameHour)
            var dfOutput = mergeTwoHours1(sparkSession,dfPre,dfCurr)//merge giờ cũ và giờ mới
            var index1 = listPathHourGroup.head.lastIndexOf("\\")
            val namNewHour = listPathHourGroup.head.substring(index1+1,index1+12)
            dfOutput.write.mode("overwrite").parquet(pathOutput+"/"+namNewHour)
            writeLog(pathFileLog,listPathHourGroup)
            writeLogRecently(pathFileRecentlyLog,listPathHourGroup.head)
            listPathHourGroup = List[String]()
          }
        }
      }
      catch {
        case e:Exception=>{
          println(e.getCause.getMessage)
        }
      }
    }
  }
  //ghi lại đường dẫn những file input đã xử lý để khi đọc lại không xử lý lại những file này nữa
  def writeLog(pathLog:String,listPath:List[String]): Unit ={
    listPath.foreach(path=>{
      reflect.io.File(pathLog).appendAll(path)
      reflect.io.File(pathLog).appendAll("\n")
    })
  }
  //ghi lại đường dẫn lưu output mới nhất -> nếu crash sẽ lấy ouput này để join với file input đầu vào
  def writeLogRecently(pathLogRecently:String,path:String): Unit ={
    reflect.io.File(pathLogRecently).writeAll(path)
  }

  def main(args: Array[String]): Unit = {
    Logger.getLogger("org").setLevel(Level.ERROR)
    val sparkSession:SparkSession = SparkSession.builder()
      .appName("test")
      .config("spark.sql.inMemoryColumnarStorage.compressed", true)
      .config("spark.sql.shuffle.partitions",30)
      .config("spark.sql.autoBroadcastJoinThreshold",-1)
      .master("local[6]")
      .getOrCreate()
//    val pathLog ="E:/Tài liệu/Big Data/log"
//    val pathInput = "E:/Tài liệu/Big Data/mdo"
//    val pathOutput = "E:/Tài liệu/Big Data/output"
//    val pathLogRecently = "E:/Tài liệu/Big Data/log"
    val pathLog = "/u01/app-mapping/log"
    val pathInput = "/u01/data/collect/mdo/YHA_Radius/done"
    val pathOutput = "/u01/app-mapping/done"
    val pathLogRecently = "/u01/app-mapping/log"
    readFile(sparkSession,pathInput,pathOutput,pathLog,pathLogRecently)

    //    val threadReadFile = new Thread {
    //      readFile(sparkSession,pathInput,pathOutput,pathLog,pathLogRecently)
    //    }
    //    threadReadFile.start()
  }
}
